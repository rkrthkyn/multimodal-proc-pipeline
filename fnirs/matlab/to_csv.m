files = dir;
file_path = '/home/rohith/Insync/rohithkarthikeyan@tamu.edu/Google Drive - Shared drives/tDCS: Closed Loop Neurotech/Data/[Pilot] Prediction Modeling/PVT/instruments/tDCS/YiZh/fNIRS/NULL';

for i = 3:length(files)
    
    load(files(i).name);
    out_data = procResult.dc;
    new_data = [];
    
    for k = 1:12
        new_data = horzcat(new_data, out_data(:,:,k));
    end
    
    new_data = horzcat(new_data, s);
    new_data = horzcat(new_data, t);

    csvwrite(strcat(files(i).name, '.txt'), new_data)
    csvwrite(strcat(files(i).name, '.csv'), SD.MeasList)

end

