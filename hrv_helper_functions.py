import time
import datetime
from sklearn.preprocessing import MinMaxScaler
from hrvanalysis import remove_outliers, interpolate_nan_values, remove_ectopic_beats
from hrvanalysis import extract_features
import os
import glob
import pandas as pd
import matplotlib.pyplot as plt
import difflib
import helper_functions as hf
import numpy as np


'''
- Setup independent performance and hrv feature extraction functions?
'''

def initializer():

    ibi_extension = 'txt'
    ibi_files = glob.glob('*.{}'.format(ibi_extension))

    performance_extension = 'csv'
    performance_files = glob.glob('*.{}'.format(performance_extension))

    ''' features of interest '''
    features_df = pd.DataFrame(columns = ['sdnn', 'pnn50', 'rmssd', 'lf', 'vlf', 'hf', 'lf_hf_ratio', 'sd1', 'sd2', 'samp_en', 'feature_time_stamp'])
    perf_df = pd.DataFrame(columns = ['std','delay', 'time_stamp'])

    return ibi_files, performance_files, features_df, perf_df

# function to convert ibi time stamp to seconds
def ibi_time_to_seconds(input):
    x = time.strptime(input.split('.')[0],'%H:%M:%S')
    output = datetime.timedelta(hours=x.tm_hour,minutes=x.tm_min,seconds=x.tm_sec).total_seconds()
    output = output+int(input.split('.')[1])/1000 # include ms precision
    return output

# function to convert performance data file time stamp to seconds
# TODO: include ms detail in performance file metadata
def performance_time_to_seconds(input):
    x = time.strptime(input,'%H-%M-%S')
    output = datetime.timedelta(hours=x.tm_hour,minutes=x.tm_min,seconds=x.tm_sec).total_seconds()
    return output

# filter performance data for outliers
# TODO: confirm outlier definition in-terms of quantiles

def get_response_data(in_data, rm_out):

    q_low = in_data["response_delay"].quantile(0.1)
    q_hi  = in_data["response_delay"].quantile(0.9)

    if rm_out == True:
        new_df = in_data[(in_data["response_delay"] < q_hi) & (in_data["response_delay"] > q_low)]
    else: 
        new_df = in_data

    new_df = new_df.dropna(subset=['response_delay'])
    new_df = new_df.drop(new_df.index[new_df['incorrect_response']==1])
    
    x = new_df['absolute_response_time']    
    y = new_df['response_delay']
    
    return x,y

# do minmax scaling on input
def feature_scaling(input):
    input = input.values.reshape(-1,1)
    scaler = MinMaxScaler()
    input_scaled = scaler.fit_transform(input)
    return input_scaled

# fit regularized spline to data
def fit_reg_spline(input_x, input_y):
    x_range = np.arange(input_x.min(),input_x.max(), 0.5)
    spline_model = hf.get_natural_cubic_spline_model(input_x, 
    input_y, input_x.min(),input_x.max(), n_knots=3)
    y_est = spline_model.predict(x_range)
    return x_range, y_est

def plot_feature_data(axis, data, label, features_df, x_range_perf, y_est_perf):
    '''
    axis - axis handle
    data - df column
    '''
    scaled_data = feature_scaling(data)
    axis.scatter(features_df['feature_time_stamp'].values, scaled_data, color = '#16a085', alpha = 0.5)
    axis.plot(x_range_perf,y_est_perf, color = '#d35400', linewidth = '3', label = 'Response trend', linestyle = '--')
    axis.set_ylabel(label)

    x_range_hrv, y_est_hrv = fit_reg_spline(features_df['feature_time_stamp'], scaled_data)
    axis.plot(x_range_hrv,y_est_hrv, color = '#2980b9', linewidth = '3', label = 'IBI trend', linestyle = '--', alpha= 0.35)

def get_file_match(f, all_files):
    for element in all_files:
        if f[:-4] == element[:-4]:
            matched_file = element
    return matched_file

def extract_hrv_features(f, in_df, performance_files, perf_df, del_t, normalize = False):

    '''
    Input: 

    f - file name
    in_df - df template for features
    
    Output:

    in_df - extracted features as df
    perf_df - time synchronized performance data
    ibi new - time synchronized ibi data
    '''
    
    ''' set required variables here '''
    end_of_df = False
    init_filter = 25
    out_filter = 100

    ibi_data = pd.read_csv(f,sep = '\t', lineterminator='\n', header = 2)   
    ibi_data.columns = ["time", "ibi"] 
    ibi_data["time"] = ibi_data['time'].apply(ibi_time_to_seconds)
    ibi_start_time = ibi_data['time'][0] # in seconds
    
    # find csv file that matches IBI file
    matched_file = get_file_match(f, performance_files)   
    file_record_time_stamp = matched_file.split('.csv')[0]
    file_record_time_stamp = file_record_time_stamp[-8:]
    file_record_time_stamp = performance_time_to_seconds(file_record_time_stamp)
    
    # check what started first, IBI/ performance?
    start_time_diff = ibi_start_time - file_record_time_stamp
    ibi_data['time'] = ibi_data['time'] - ibi_start_time
    performance_data = pd.read_csv(matched_file, sep = ',')
    
    # synchronize ibi w/performance data
    if start_time_diff > 0:
        ibi_data['time'] = ibi_data['time']-start_time_diff
    elif start_time_diff < 0:
        performance_data['absolute_response_time'] = performance_data['absolute_response_time']-start_time_diff
    else:
        pass    
    
    # remove nan responses to performance 
    performance_data = performance_data.dropna()
    # pre-processing on IBI data
    rr_intervals_without_outliers = remove_outliers(rr_intervals=ibi_data['ibi'].values,  low_rri=300, high_rri=1200) #TODO: find rationale/ better cutoffs
    rr_intervals_interpolated = interpolate_nan_values(rr_intervals=rr_intervals_without_outliers, interpolation_method="linear")
    nn_intervals = remove_ectopic_beats(rr_intervals=rr_intervals_interpolated, method="malik")    
    nn_intervals  = interpolate_nan_values(rr_intervals=nn_intervals, interpolation_method="linear")
    ibi_data['nn_intervals'] = nn_intervals
    ibi_data = ibi_data[ibi_data['nn_intervals'].notna()]
    
    # TODO: improve outlier removal criteria
    ibi_new = ibi_data[init_filter:-out_filter].copy()
    ibi_new = ibi_new.reset_index()

    # bin data into three minute intervals for analysis
    
    start = ibi_new['time'][0]
    stop = start+del_t
    
    
    while not(end_of_df):
    
        if stop > ibi_new['time'].iloc[-1]:
            stop = ibi_new['time'].iloc[-1]          
            end_of_df = True
            print('end-of-file')

        interval = ibi_new['time'].between(start,stop, inclusive = True)
        performance_interval = performance_data['absolute_response_time'].between(start, stop, inclusive = True)
        
        ''' 
            do analysis on interval data provide time stamp as median 
            - Time domain features (SDNN, pNN50, RMSSD)
            - Frequency domain features (LF, HF, LF/HF)
            - Non-linear domain features (sd1, sd2, sampen)
        '''  
       
        interval_nn = ibi_new['nn_intervals'][interval]
        interval_time = ibi_new['time'][interval]

        if interval_nn.size < 20:
            end_of_df = True
            print('forced end-of-file')

        else:

            performance_interval_delay = performance_data['response_delay'][performance_interval]
            performance_interval_time = performance_data['absolute_response_time'][performance_interval]
            perf_df  = perf_df.append({'delay':performance_interval_delay.mean(),'std': performance_interval_delay.std() ,'time_stamp': performance_interval_time.median(skipna=True)}, ignore_index = True)
            
            # get time domain features
            time_domain_features = extract_features.get_time_domain_features(list(interval_nn))
        
            # get frequency domain features
            freq_domain_features = extract_features.get_frequency_domain_features(list(interval_nn))
        
            # get non-linear domain features
            poincare_plot_features = extract_features.get_poincare_plot_features(list(interval_nn))
            # get sample entropy
            sampen = extract_features.get_sampen(list(interval_nn))
            
            # update vars into df
            in_df = in_df.append({'sdnn':time_domain_features['sdnn'],'pnn50': time_domain_features['pnni_50'],
                'rmssd': time_domain_features['rmssd'],'vlf': freq_domain_features['vlf'],'hf':freq_domain_features['hf'],
                'lf':freq_domain_features['lf'], 'lf_hf_ratio': freq_domain_features['lf_hf_ratio'], 'sd1': poincare_plot_features['sd1'],
                'sd2': poincare_plot_features['sd2'], 'samp_en':sampen['sampen'], 'feature_time_stamp': interval_time.median()},ignore_index = True)

        start = stop 
        stop = start+del_t

    # normalize column for each participant

    if normalize:
        perf_df = (perf_df-perf_df.min()) / (perf_df.max()-perf_df.min())
        in_df =   (in_df-in_df.min()) / (in_df.max()-in_df.min())
    
    return in_df, performance_data, ibi_new, perf_df
